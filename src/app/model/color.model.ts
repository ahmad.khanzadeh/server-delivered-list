export interface colorDataModel{
    page: number
    per_page: number,
    total:number,
    total_page:number,
    data: colorModel[],
    support: colorSupport
    
    
    }
    export interface colorModel{
        id: number,
        email: string,
        first_name: string,
        last_name: string,
        avatar: string
    
    }
    export interface colorSupport{
        url: any,
        text: any,
    }