export interface UserDataModel{
page: number
per_page: number,
total:number,
total_page:number,
data: UserModel[],
support: UserSupport


}
export interface UserModel{
    id: number,
    email: string,
    first_name: string,
    last_name: string,
    avatar: string

}
export interface UserSupport{
    url: any,
    text: any,
}