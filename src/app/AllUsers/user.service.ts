import { environment } from '../../environments/environment';
import { UserDataModel } from '../model/user.model';
import { Injectable } from '@angular/core';
import{HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import{Observable, throwError} from 'rxjs';
import{catchError, retry} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
// export class UserListService extends DataService{
  export class UserListService {
 private userUrl= '/api/users';
//  for registerating a new member
 private url='https://reqres.in/api/users';
  httpOptions={
    
    headers: new HttpHeaders({'content-type':'application/json'})
  };

  constructor(private  http : HttpClient) { 

  }

  getUsers(page): Observable <UserDataModel>{
    
    let theParams: HttpParams= new HttpParams();
    theParams = theParams.set('page',page);
    return this.http.get<UserDataModel>(environment.apiUrl +this.userUrl,{params:theParams}).pipe(retry(3),catchError(this.showError))
  }
  getDetailUser(id:number){
   
    return this.http.get<any>(environment.apiUrl +this.userUrl+'/'+id)
    // At first< I add a subscription to the upper return, which is called : .map( combined =>{thsi.data=combined})
    // it was all wrone!
    //  I'm allowed to send just likns in the service.ts and in components< getting data from the link......
  }
  
  showError(errorResponse : HttpErrorResponse){
    if(errorResponse.error instanceof ErrorEvent){
      console.error('client side error', errorResponse.error.message);
    }else{
      console.error('Server side error', errorResponse.error.message);
    }
    return throwError('this error occured: ')
  }

  createPost(userInputs){
    // dont copy the exact codes of the component. check address 
    //  A (this) was extra( no Items found for that)
    // return this.http.post(this.url,this.userInputs);
    return this.http.post(this.url,userInputs);
  }
}
