import { usernameValidators } from './username.Validator';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { AddmemberService } from './../../addmember.service';
import {UserListService} from '../user.service';

@Component({
  selector: 'app-addmember',
  templateUrl: './addmember.component.html',
  styleUrls: ['./addmember.component.scss']
})
export class AddmemberComponent implements OnInit {
  // Self trainign esfan
 
  
  // Project code 
    jobForm = new FormGroup({
      username:new FormControl('',[ Validators.required, Validators.minLength(3),usernameValidators.cannotContainSpace]),
      jobTitle:new FormControl('', Validators.required)
    })
    // username Validator
    get username(){
      return this.jobForm.get('username')
    }
    get theJob(){
      return this.jobForm.get('jobTitle')
    }


    // server address
    url='https://reqres.in/api/users';
    memberJsonHolder;
    constructor(private AddmemberService: UserListService){
      // this.http.post(this.url,this.userInputs).toPromise().then(responseData =>{
      //   console.log("the server response for post is as below: ");
      //   console.log(responseData);
      //   this.memberJsonHolder=JSON.stringify(responseData);
      // })
      
    }
    
  ngOnInit(): void {
   
  }

  // submiting data to server
  userInputs: any;
  // private thehttp: HttpClient;
  jobSubmit(){
    
    console.log(this.jobForm.value);
    this.userInputs=this.jobForm.value;
    this.AddmemberService.createPost(this.userInputs).subscribe(responseData =>{
      console.log("the server response for post is as below: ");
      console.log(responseData);
      this.memberJsonHolder=JSON.stringify(responseData);
      // I search in stack overflow: how to clear a jobForm after submision and the following code was found!
      this.jobForm.reset();
      // this.jobForm.patchValue(value[1],{'',emitEvent});
    })
  }
}
