import { UserListService } from '../user.service';
import { Component, OnInit } from '@angular/core';
import { Routes, ActivatedRoute } from '@angular/router';
import { UserDataModel,UserModel } from '../../model/user.model';



@Component({
  selector: 'app-each-user',
  templateUrl: './each-user.component.html',
  styleUrls: ['./each-user.component.scss']
})
export class EachUserComponent implements OnInit {
 hname: string;
 hlastName: string;
 hemail: string;
 hpic: string;
 humanData: UserModel = null;

  constructor(private route: ActivatedRoute,private service: UserListService) { }

  ngOnInit(){
   
    let way=this.route.snapshot.paramMap;
    let userId =this.route.snapshot.paramMap.get('userId');
    console.log(userId);
    this.service.getDetailUser(+userId).subscribe(data => {
        this.humanData=data.data;
        // when mina asks me to watch the network tab in the inspect of my browser, I find out that the data which I subscribe to, 
        // is a big object contains of the other Object. First one is data and second one is support.
        // For getting info, I should save : data.data !!!!!1
       }
    )
  }
 

}
