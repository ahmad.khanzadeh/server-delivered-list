import { UserDataModel, UserModel } from '../../model/user.model';
import { Component, OnInit } from '@angular/core';
import { Routes } from '@angular/router';
import{UserListService} from '../user.service'

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
 
  users: UserModel[]= [];
  usersData: UserDataModel = null;
  persons: any[];
  hide : boolean= true;
  page : number = 1;
  lastPage : number = 0;
  isShown: boolean = true;
  showButton: boolean= false;
  
  constructor( private userListService: UserListService){ }

  ngOnInit(){
    this.getUsers();
    console.log('First detected')
    
  };

  getUsers(){
    this.userListService.getUsers(this.page).subscribe(
      data => {
        if(data){
        this.usersData= data;
        // how to concat an array in ES6.....
        this.users=[...this.users,... data.data];
        if(this.usersData.total>this.users.length){
          this.showButton = true;
        }else{
          this.showButton=false;
        }
        
      }
        else{
          this.showButton = false;
        }
      
      } 
      )
  }
  show(){
    this.page+=1;
    this.getUsers();
    console.log("button is clicked!")
    
  }
 

}
