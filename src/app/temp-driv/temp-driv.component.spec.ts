import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TempDrivComponent } from './temp-driv.component';

describe('TempDrivComponent', () => {
  let component: TempDrivComponent;
  let fixture: ComponentFixture<TempDrivComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TempDrivComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TempDrivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
