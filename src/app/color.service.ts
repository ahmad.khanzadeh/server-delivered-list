import { environment } from './../environments/environment';
import { colorDataModel, colorModel } from './model/color.model';
import { Injectable } from '@angular/core';
import{HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import{Observable, throwError} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class ColorService {
  private colorUrl= '/api/unknown';
  private apiUrl='https://reqres.in'
  httpOptions={
    
    headers: new HttpHeaders({'content-type':'application/json'})
  };
  constructor(private  http : HttpClient) {}
  getPeople(page): Observable <colorDataModel>{
      let theParams: HttpParams= new HttpParams();
      theParams = theParams.set('page',page);
      return this.http.get<colorDataModel>(environment.apiUrl +this.colorUrl,{params:theParams})
  }
  getDetailUser(id:number){
   
    return this.http.get<any>(this.apiUrl +this.colorUrl+'/'+id)
    // At first< I add a subscription to the upper return, which is called : .map( combined =>{thsi.data=combined})
    // it was all wrone!
    //  I'm allowed to send just likns in the service.ts and in components< getting data from the link......
  }

   
}
