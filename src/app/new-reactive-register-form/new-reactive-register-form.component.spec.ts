import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewReactiveRegisterFormComponent } from './new-reactive-register-form.component';

describe('NewReactiveRegisterFormComponent', () => {
  let component: NewReactiveRegisterFormComponent;
  let fixture: ComponentFixture<NewReactiveRegisterFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewReactiveRegisterFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewReactiveRegisterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
