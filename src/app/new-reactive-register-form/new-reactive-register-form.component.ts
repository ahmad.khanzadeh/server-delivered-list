// personal Validators
import { Usernamevalidators } from './username.validators';
import { phoneNumberValidator } from './phoneNumber.Validator';

//import { Component, OnInit } from '@angular/core';
import { Component, } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';


@Component({
  selector: 'app-new-reactive-register-form',
  templateUrl: './new-reactive-register-form.component.html',
  styleUrls: ['./new-reactive-register-form.component.scss']
})
// export class NewReactiveRegisterFormComponent implements OnInit {
export class NewReactiveRegisterFormComponent {
  profileForm: FormGroup;
  name: FormControl;
  userName:FormControl;
  mobileN:FormControl;
  thePassword:FormControl;
  theEmail:FormControl;
  userInputs: any;
  // jobName: any

  // select Box 
  isSubmitted = false;
  emptyJobs: any = ['Web developer', 'Product Manager', ' accounter', 'janitor']

  mobilePattern: RegExp = /^[0][9][0-9]{9}/;
 // Emailpattern: RegExp= /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    Emailpattern:  RegExp = /^[a-zA-Z0-9](\.?[a-zA-Z0-9_-]){0,}@[a-zA-Z0-9-]+\.([a-zA-Z]{1,6}\.)?[a-zA-Z]{2,6}$/;
  // توابعی که عمل گت رو برامون انجام بدن
  // نمی دونم چرا این فرم های داخل بخش زیری کار نمی کنن 

  constructor(public fb: FormBuilder) {}

  ngOnInit(): void {

    this.name = new FormControl('', [Validators.required, Validators.minLength(3)]),
    this.userName= new FormControl('', [Validators.required, Validators.minLength(3), Usernamevalidators.cannotContainSpace]),
    this.mobileN= new FormControl('', [Validators.required, Validators.minLength(11), Validators.pattern(this.mobilePattern), phoneNumberValidator.justNumberAccepted]),
    this.thePassword= new FormControl('', [Validators.required, Validators.minLength(5)]),
    this.theEmail= new FormControl('',[Validators.required,Validators.pattern(this.Emailpattern)]),
    // this.jobTitle=new FormControl('', [Validators.required]),
   


    this.profileForm = new FormGroup({
      // این زیری ها همه = بودن ولی مجبور شدم : بذارم تا ران بشه 
      name : this.name,
      userName : this.userName,
      //theEmail : new FormControl(''),
      theEmail : this.theEmail,
      mobileN : this.mobileN,
      jobTitle : new FormControl(''),
      // jobTitle : this.jobTitle,
      thePassword : this.thePassword,
    })
     this.setDefaults();
  }
   setDefaults() {
    //  اسم همین فرمی که ساخته بودیم رو تو پرانتز اول باید می ذاشتیم 
     this.profileForm.get("jobTitle").patchValue(this.emptyJobs[1]);
   }

   
    // selectBox part
     registrationForm = this.fb.group({
       jobTitle: ['', [Validators.required]]
     })
    // Choose city using select dropdown
    changeJob(e) {
      console.log(e.value)
      this.jobTitle.setValue(e.target.value, {
        onlySelf: true
      })
    }
  
    // Getter method to access formcontrols
    get jobTitle() {
      return this.registrationForm.get('jobTitle');
      // اینجا هم قراره که همین فرمی که ساختیم رو گت کنیم ( تو صفحه زدم کدومه )
    }
  // end Select Box 

  onSubmit() {
    console.log(this.profileForm.value)
    // TODO: Use EventEmitter with form value
    this.userInputs=this.profileForm.value;
    // for SelectBox:
        this.isSubmitted = true;
         if (!this.registrationForm.valid) {
            return false;
        } else {
             alert(JSON.stringify(this.registrationForm.value))
    }

  }
  
  updateName() {
    this.name.setValue('empty box from user');
  }
}
