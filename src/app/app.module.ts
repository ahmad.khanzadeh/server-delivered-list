import { NewReactiveRegisterFormComponent } from './new-reactive-register-form/new-reactive-register-form.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import{ RouterModule} from '@angular/router'

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserListComponent } from './AllUsers/user-list/user-list.component';
import{UserListService} from './AllUsers/user.service';
// import { PersonsComponent } from './persons/persons.component';
// import { ReactiveFormsModule } from '@angular/forms';
// import { RouterModule } from '@angular/router'
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';
import { EachUserComponent } from './AllUsers/each-user/each-user.component';
import { UserNotFoundComponent } from './user-not-found/user-not-found.component';
import { ColorReciverComponent } from './colors/color-reciver/color-reciver.component';
import {ColorService} from './color.service';
import { NavbarComponent } from './navbar/navbar.component';
import { ChildColorComponent } from './colors/color-reciver/child-color/child-color.component';
import { AddmemberComponent } from './AllUsers/addmember/addmember.component';
import { TempDrivComponent } from './temp-driv/temp-driv.component'
// import { AddmemberService } from './addmember.service';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    // PersonsComponent,
    EachUserComponent,
    UserNotFoundComponent,
    NewReactiveRegisterFormComponent,
    ColorReciverComponent,
    NavbarComponent,
    ChildColorComponent,
    AddmemberComponent,
    TempDrivComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: 'users', component: UserListComponent },
      {path:'', redirectTo: 'users' ,pathMatch: 'full'},
      {path:'users/:userId', component: EachUserComponent },
      {path:'forms', component: NewReactiveRegisterFormComponent },
      {path:'colors', component: ColorReciverComponent},
      {path:'addmember', component: AddmemberComponent},
      {path: '**', component: UserNotFoundComponent }
      // wild card
      //  in an array....
    ]
    )
   

  ],
  providers: [
    UserListService,
    ColorService,
    // AddmemberService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
