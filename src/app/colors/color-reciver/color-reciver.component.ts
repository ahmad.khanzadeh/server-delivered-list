import { colorDataModel, colorModel } from './../../model/color.model'; 
import { Component, OnInit } from '@angular/core';
import {ColorService} from './../../color.service'


@Component({
  selector: 'app-color-reciver',
  templateUrl: './color-reciver.component.html',
  styleUrls: ['./color-reciver.component.scss']
})
export class ColorReciverComponent implements OnInit {
  coloredUsers: colorModel[]= [];
  usersData: colorDataModel = null;
  page : number = 1;
  showButton: boolean= false;
  backColor:number =null;

// the data from child, recived by @output
  selectedColor='';


  selectCounter:number=0;
  colorsCarrear: string[]=[];
  colorName: string= this.selectedColor;
  letCollect: boolean= true;
  getSelectedColor(event){
    // the Array counted from 0 so if I want to coolect 3 object, I should count up to 2!
    if (this.selectCounter<=2){
    console.log(event);
    this.colorsCarrear.push(event);
    this.selectedColor='';
    this.selectCounter=this.colorsCarrear.length;
    this.letCollect=true;
    console.log(this.letCollect);
    }
    else{
      console.log(event)
      this.letCollect=false;
      console.log(this.letCollect);
      // data is collected Entirely. dont add any more object==disabled the button or hide it by letCollect property
    }
  }


  constructor(private coloredUser: ColorService ) { }

  ngOnInit(): void {
    this.getWriters()
    this.selectCounter=this.selectedColor.length;
  }
  getWriters(){
    this.coloredUser.getPeople(this.page).subscribe(data =>{
      if(data){
        this.usersData= data;
        // how to concat an array in ES6.....
        this.coloredUsers=[...this.coloredUsers,... data.data];
        console.log(this.coloredUser);
        if(this.usersData.total>this.coloredUsers.length){
          this.showButton = true;
        }else{
          this.showButton=false;
        }
      }
      else{
        this.showButton = false;
      }
    })
  }
  show(){
    
    this.page+=1;
    this.getWriters();
    console.log("button is clicked!")
    
  }

}
