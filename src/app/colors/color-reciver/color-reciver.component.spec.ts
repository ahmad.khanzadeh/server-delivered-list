import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorReciverComponent } from './color-reciver.component';

describe('ColorReciverComponent', () => {
  let component: ColorReciverComponent;
  let fixture: ComponentFixture<ColorReciverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorReciverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorReciverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
