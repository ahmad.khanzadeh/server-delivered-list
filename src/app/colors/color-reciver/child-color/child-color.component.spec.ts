import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildColorComponent } from './child-color.component';

describe('ChildColorComponent', () => {
  let component: ChildColorComponent;
  let fixture: ComponentFixture<ChildColorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildColorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
