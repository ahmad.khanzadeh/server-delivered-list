import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';
// ** new learn**

// import * as EventEmitter from 'events';

@Component({
  selector: 'app-child-color',
  templateUrl: './child-color.component.html',
  styleUrls: ['./child-color.component.scss']
})
export class ChildColorComponent implements OnInit {

  @Input ('parentData') public theColor;
  // selectedColor= theColor;
  @Input ('parmisionData') public btnPermision;

  @Output() public childData= new EventEmitter();



  callEvent(){
    this.childData.emit(this.theColor.color)
 
  }
  constructor() { }

  ngOnInit(): void {
  }


}
